# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:vb9606/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/vb9606/stroboskop/commits/f87d5d7faec59a5e8ea8b5f4efe7073efbfa4b51

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/vb9606/stroboskop/commits/e86038f5604c56f7dcc8b0c3953c5b84d7d34ac7

Naloga 6.3.2:
https://bitbucket.org/vb9606/stroboskop/commits/2494666805eaa8bc44dc3ceddfb8a3646136f00d

Naloga 6.3.3:
https://bitbucket.org/vb9606/stroboskop/commits/f69697d2d14ff9b70cff0eb0810a645f1adf4aa2

Naloga 6.3.4:
https://bitbucket.org/vb9606/stroboskop/commits/7b8667113a093faffde4583218db6419d3341b62

Naloga 6.3.5:

```
git checkout master
git merge izlet
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/vb9606/stroboskop/commits/7b55e0a247f86b15b1c9194b5f723fb0931a4eaf

Naloga 6.4.2:
https://bitbucket.org/vb9606/stroboskop/commits/cd2d21c0ec629091e768cd94d0cdc7ccc09ee670

Naloga 6.4.3:
https://bitbucket.org/vb9606/stroboskop/commits/5e5cf3d71a5cba9546bbf09a18288d0e37701cc8

Naloga 6.4.4:
https://bitbucket.org/vb9606/stroboskop/commits/283b09c0943185448fd013721661a4981386f064